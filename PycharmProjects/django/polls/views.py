from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404

# Create your views here.
from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.views.generic import View
from polls.forms import QuestionForm, ChoiceForm
from polls.models import Question, Choice


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('pub_date')


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'




#Results view class for votes results
class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


# Question view for list all of Questions
class QuestionView(View):
    # get method 
    def get(self, request):
        question_form = QuestionForm()
        return render(request, 'polls/addQuestion.html', {
            'question_form': question_form,
        })

    def post(self, request):
        question_form = QuestionForm(request.POST)
        if question_form.is_valid():
            question_form.save()
            return HttpResponseRedirect(reverse('polls:index'))
        else:
            return render(request, 'polls/addQuestion.html', {
                'question_form': question_form,
                'errors': question_form.errors
            })

#Choice view for create a new choice
class ChoiceView(View):
    # get and post same part
    def dispatch(self, request, question_id, *args, **kwargs):
        self.question = Question.objects.get(pk=question_id)
        return super(ChoiceView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        choice_form = ChoiceForm(initial={'question': self.question})
        return render(request, 'polls/addChoice.html', {
            'question': self.question,
            'choice_form': choice_form,
        })

    def post(self, request, *args, **kwargs):
        choice_form = ChoiceForm(request.POST, initial={'question': self.question})
        if choice_form.is_valid():
            choice_form.save()
            return HttpResponseRedirect(reverse('polls:detail', args=(choice_form.cleaned_data.get('question').pk,)))
        else:
            return render(request, 'polls/addChoice.html', {
                'question': self.question,
                'choice_form': choice_form,
                'errors': choice_form.errors
            })

#Voteview for votes and post it
# get method are not using this view
class VoteView(View):
    def __init__(self):
        self.output = ''

    def dispatch(self, request, *args, **kwargs):
        return super(VoteView, self).dispatch(request, *args, **kwargs)

    def get(self, request, question_id):
        raise Http404("Not Found")

    def post(self, request, question_id, *args, **kwargs):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            # Redisplay the question voting form.
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "You didn't select a choice."
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if a
            # user hits the Back button.
            return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))