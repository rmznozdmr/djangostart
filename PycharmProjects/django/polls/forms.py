__author__ = 'discovery'
from django import forms
from polls.models import Question, Choice



class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        #form for questions text box date is auto
        fields = ['question_text']



class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        #form for choice
        fields = ['question', 'choice_text']


