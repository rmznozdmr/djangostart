import datetime
from django.db import models
from django.utils import timezone

# Create your models here.

#Question models it has a name and date
class Question(models.Model):
    question_text = models.CharField(verbose_name="Question", max_length=500)
    pub_date = models.DateTimeField("Publish Date", auto_now_add=True)

    # this method get us Quesiton class name
    def __str__(self):
        return self.question_text
    #property means call method like a variable
    @property
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

# Choice model it has a question relational , choice name and votes number
class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    #unicode is same _str_
    def __unicode__(self):
        return self.choice_text


