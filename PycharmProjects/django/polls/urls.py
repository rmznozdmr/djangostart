__author__ = 'discovery'
from django.conf.urls import url



from django.conf.urls import url



from . import views

# all url patterns
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>[0-9]+)/vote/$',views.VoteView.as_view(), name='vote'),
    url(r'^question/$', views.QuestionView.as_view(), name='question'),
    url(r'^choice/(?P<question_id>\d+)/$', views.ChoiceView.as_view(), name='choice'),
    #url(r'^(?P<pk>[0-9]+)/vote/$', views.VotesView.as_view(), name='vote'),
]