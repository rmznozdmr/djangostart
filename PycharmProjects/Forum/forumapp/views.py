import profile
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404

# Create your views here.
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from django.views import generic
from django.views.generic import View
from forumapp.forms import CategoriesForm, TopicsForm,UserForm,CommentsForm
from forumapp.models import Categories, Topics,User

from django.contrib.auth.models import User
from django import forms



class IndexView(generic.ListView):

    model = Categories
    template_name = 'index.html'
    context_object_name = 'latest_categories_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Categories.objects.order_by('pub_date')


class DetailView(generic.DetailView):
    model = Categories
    template_name = 'category.html'




#Results view class for votes results
class ResultsView(generic.DetailView):
    model = Categories
    template_name = 'results.html'


# Question view for list all of Questions
class CategoriesView(View):
    # get method 
    def get(self, request):
        categories_form = CategoriesForm()
        return render(request, 'create_cat.html', {
            'categories_form': categories_form,
        })

    def post(self, request):
        categories_form = CategoriesForm(request.POST)
        if categories_form.is_valid():
            categories_form.save()
            return HttpResponseRedirect(reverse('forumapp:index'))
        else:
            return render(request, 'create_cat.html', {
                'categories_form': categories_form,
                'errors': categories_form.errors
            })

#Choice view for create a new choice
class TopicView(View):
    # get and post same part
    def dispatch(self, request, question_id, *args, **kwargs):
        self.categories = Categories.objects.get(pk=question_id)
        return super(TopicView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        choice_form = TopicsForm(initial={'categories': self.categories})
        return render(request, 'create_topic.html', {
            'categories': self.categories,
            'choice_form': choice_form,
        })

    def post(self, request, *args, **kwargs):
        choice_form = TopicsForm(request.POST, initial={'categories': self.categories})
        if choice_form.is_valid():
            choice_form.save()
            return HttpResponseRedirect(reverse('forumapp:index', args=(choice_form.cleaned_data.get('categories').pk,)))
        else:
            return render(request, 'create_topic.html', {
                'categories': self.categories,
                'choice_form': choice_form,
                'errors': choice_form.errors
            })


class CommentView(View):
    # get method
    def get(self, request):
        comment_form = CommentsForm()
        return render(request, 'create_cat.html', {
            'categories_form': comment_form,
        })

    def post(self, request):
        comment_form = CommentsForm(request.POST)
        if comment_form.is_valid():
            comment_form.save()
            return HttpResponseRedirect(reverse('forumapp:index'))
        else:
            return render(request, 'create_cat.html', {
                'categories_form': comment_form,
                'errors': comment_form.errors
            })
#Voteview for votes and post it
# get method are not using this view
# class VoteView(View):
#     def __init__(self):
#         self.output = ''
#
#     def dispatch(self, request, *args, **kwargs):
#         return super(VoteView, self).dispatch(request, *args, **kwargs)
#
#     def get(self, request, question_id):
#         raise Http404("Not Found")
#
#     def post(self, request, question_id, *args, **kwargs):
#         question = get_object_or_404(Question, pk=question_id)
#         try:
#             selected_choice = question.choice_set.get(pk=request.POST['choice'])
#         except (KeyError, Choice.DoesNotExist):
#             # Redisplay the question voting form.
#             return render(request, 'polls/detail.html', {
#                 'question': question,
#                 'error_message': "You didn't select a choice."
#             })
#         else:
#             selected_choice.votes += 1
#             selected_choice.save()
#             # Always return an HttpResponseRedirect after successfully dealing
#             # with POST data. This prevents data from being posted twice if a
#             # user hits the Back button.
#             return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

def register(request):
    # Like before, get the request's context.
    context = RequestContext(request)

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)


        # If the two forms are valid...
        if user_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set commit=False.
            # This delays saving the model until we're ready to avoid integrity problems.


            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.

            profile.save()

            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print user_form.errors

    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()


    # Render the template depending on the context.
    return render_to_response(
            'register.html',
            {'user_form': user_form,'registered': registered},
            context)