from django.db import models

# Create your models here.
import datetime
from django.db import models
from django.utils import timezone

# Create your models here.
class User(models.Model):
    #user_id =models.PositiveIntegerField(verbose_name="User ID")
    user_name = models.CharField(verbose_name="User Name", max_length=500)
    user_pass = models.CharField(verbose_name="Password", max_length=500)
    user_mail = models.EmailField(verbose_name="E-Mail", max_length=500)
    user_date = models.DateTimeField("Publish Date", auto_now_add=True)

    def __str__(self):
        return self.user_name

#Question models it has a name and date
class Categories(models.Model):
    #user = models.ForeignKey(User)
    #cat_id =models.PositiveIntegerField(verbose_name="Cat ID")
    cat_name = models.CharField(verbose_name="Categories", max_length=500)
    cat_description = models.CharField(verbose_name="Categories Description", max_length=500)
    pub_date = models.DateTimeField("Publish Date", auto_now_add=True)

    # this method get us Quesiton class name
    def __str__(self):
        return self.cat_name
    #property means call method like a variable
    #@property
    #def was_published_recently(self):
    #    return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

# Choice model it has a question relational , choice name and votes number
class Topics(models.Model):
    user = models.ForeignKey(User)
    categories = models.ForeignKey(Categories)
    #topic_id =models.PositiveIntegerField(verbose_name="Topic ID")
    topic_subject = models.CharField(verbose_name="Topic Subject", max_length=500)
    topic_date = models.DateTimeField("Publish Date", auto_now_add=True)
    #topic_cat =models.PositiveIntegerField(verbose_name="Topic Cat")
    #topic_by =models.PositiveIntegerField(verbose_name="Topic By")

    # this method get us Quesiton class name
    def __str__(self):
        return self.topic_subject


class Posts(models.Model):
    user = models.ForeignKey(User)
    topics = models.ForeignKey(Topics)
    #posts_id =models.PositiveIntegerField(verbose_name="Post ID")
    posts_content = models.CharField(verbose_name="Post Content", max_length=500)
    posts_date = models.DateTimeField("Post Date", auto_now_add=True)
    #posts_topic =models.PositiveIntegerField(verbose_name="Post Cat")
    #posts_by =models.PositiveIntegerField(verbose_name="Post By")

class Comment(models.Model):
    user= models.ForeignKey(User)

    comment= models.CharField(verbose_name="Comment ",max_length=500)

    # this method get us Quesiton class name



    #unicode is same _str_
    def __unicode__(self):
        return self.posts_content