__author__ = 'discovery'
__author__ = 'discovery'
from django.conf.urls import url



from django.conf.urls import url



from . import views

# all url patterns
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    #url(r'^(?P<question_id>[0-9]+)/vote/$',views.VoteView.as_view(), name='vote'),
    url(r'^categories/$', views.CategoriesView.as_view(), name='categories'),
    url(r'^topic/(?P<question_id>\d+)/$', views.TopicView.as_view(), name='topic'),
    url(r'^register/$', views.register, name='register'), # ADD NEW PATTERN
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
    #url(r'^(?P<pk>[0-9]+)/vote/$', views.VotesView.as_view(), name='vote'),
]