from django.contrib import admin
import time
from .models import User, Categories, Topics
#
# class Topics(admin.StackedInline):
#     model = Topics
#     extra = 1


class CategoriesAdmin(admin.ModelAdmin):
     model = Categories
     extra = 2



class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['user_name']}),
        (None,               {'fields': ['user_pass']}),
        (None,               {'fields': ['user_mail']}),
    ]
    #list_display = ('question_text', 'pub_date', 'was_published_recently')
    #inlines = [Categories]
    #inlines = [Topics]

admin.site.register(User, UserAdmin)
admin.site.register(Categories)
admin.site.register(Topics)