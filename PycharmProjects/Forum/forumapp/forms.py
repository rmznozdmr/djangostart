__author__ = 'discovery'
__author__ = 'discovery'
from django import forms
from forumapp.models import Categories, Topics,User,Comment



class CategoriesForm(forms.ModelForm):
    class Meta:
        model = Categories
        #form for questions text box date is auto
        fields = ['cat_name','cat_description']



class TopicsForm(forms.ModelForm):
    class Meta:
        model = Topics
        #form for choice
        fields = ['categories', 'topic_subject']

class CommentsForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment']

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('user_name', 'user_mail', 'user_pass')

